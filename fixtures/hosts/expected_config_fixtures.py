"""This module contains fixtures for expected data which provides which are needed for testing LinuxHost class"""

import os

import pytest

from fixtures.environ_fixtures import environ
from helpers.config_utils import get_config_data


@pytest.fixture(scope="session")
def linux_host_config(request, environ: environ) -> dict:
    """The function returns the dictionary read from the config.json file."""

    return get_config_data(os.path.join(request.config.rootdir.strpath,
                                        "hosts_tests",
                                        environ.get_expected_data()))


@pytest.fixture(scope="function")
def expected_linux_host_cpu(linux_host_config: dict) -> int:
    """Fixture returns a <cpu> value from a dictionary of expected data."""

    return linux_host_config.get("cpu")


@pytest.fixture(scope="function")
def expected_linux_host_ram(linux_host_config: dict) -> int:
    """Fixture returns a <ram> value from a dictionary of expected data."""

    return linux_host_config.get("ram")


@pytest.fixture(scope="function")
def expected_linux_host_network_interfaces(linux_host_config: dict) -> int:
    """Fixture returns a <network_interfaces> value from a dictionary of expected data."""

    return linux_host_config.get("network_interfaces")


@pytest.fixture(scope="function")
def expected_linux_host_disk_size(linux_host_config: dict) -> int:
    """Fixture returns a <disk_size> value from a dictionary of expected data."""

    return linux_host_config.get("disk_size")


@pytest.fixture(scope="function")
def expected_linux_host_running_service(linux_host_config: dict) -> bool:
    """Fixture returns a <chronyd_is_running> value from a dictionary of expected data."""

    return linux_host_config.get("chronyd_is_running")


@pytest.fixture(scope="function")
def expected_linux_host_enabled_service(linux_host_config: dict) -> bool:
    """Fixture returns a <chronyd_is_enabled> value from a dictionary of expected data."""

    return linux_host_config.get("chronyd_is_enabled")
