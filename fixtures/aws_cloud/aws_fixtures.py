"""Module contains pytest fixtures for AWS Cloud modules."""

import pytest

from fixtures.environ_fixtures import environ
from models.aws_cloud.aws_instance import AWSInstance


@pytest.fixture(scope="session")
def ec2_instance(environ: environ) -> AWSInstance:
    """Fixture return created AWS Instance object"""

    return AWSInstance(environ.get_instance_id())


@pytest.fixture(scope="function")
def ec2_instance_state(ec2_instance: AWSInstance) -> str:
    """Fixture returns the <state> attribute from the AWSInstance object."""

    return ec2_instance.state


@pytest.fixture(scope="function")
def ec2_instance_instance_type(ec2_instance: AWSInstance) -> str:
    """Fixture returns the <instance_type> attribute from the AWSInstance object."""

    return ec2_instance.instance_type


@pytest.fixture(scope="function")
def ec2_instance_network_interfaces(ec2_instance: AWSInstance) -> int:
    """Fixture returns the <network_interfaces> attribute from the AWSInstance object."""

    return len(ec2_instance.network_interfaces)


@pytest.fixture(scope="function")
def ec2_instance_private_ip_address(ec2_instance: AWSInstance) -> str:
    """Fixture returns the <private_ip_address> attribute from the AWSInstance object."""

    return ec2_instance.private_ip_address


@pytest.fixture(scope="function")
def ec2_instance_root_device(ec2_instance: AWSInstance) -> str:
    """Fixture returns the <root_device> attribute from the AWSInstance object."""

    return ec2_instance.root_device


@pytest.fixture(scope="function")
def ec2_instance_root_device_type(ec2_instance: AWSInstance) -> str:
    """Fixture returns the <root_device_type> attribute from the AWSInstance object."""

    return ec2_instance.root_device_type


@pytest.fixture(scope="function")
def ec2_instance_key_pair_name(ec2_instance: AWSInstance) -> str:
    """Fixture returns the <key_pair_name> attribute from the AWSInstance object."""

    return ec2_instance.key_pair_name


@pytest.fixture(scope="function")
def ec2_instance_tags(ec2_instance: AWSInstance) -> int:
    """Fixture returns the <len 'tags'> attribute from the AWSInstance object."""

    return len(ec2_instance.tags)


@pytest.fixture(scope="function")
def ec2_instance_tag_name(ec2_instance: AWSInstance) -> str:
    """Fixture returns the <tag 'Name'> attribute from the AWSInstance object."""

    return ec2_instance.tags["Name"]
