from flask import Flask, render_template
import os

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/home")
def home():
    return <h1>Home page</h1>

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=False, port=80)
