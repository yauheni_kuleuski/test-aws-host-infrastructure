"""The module is designed to initialize fixtures with environment variables."""

import pytest

from helpers.environ_variables import EnvironVariables


@pytest.fixture(scope="session")
def environ() -> EnvironVariables:
    """
    The function returns an EnvironVariables object with environment variables available as attributes of the object.
    """

    return EnvironVariables()
