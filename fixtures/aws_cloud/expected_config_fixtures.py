""""""

import os

import pytest

from fixtures.environ_fixtures import environ
from helpers.config_utils import get_config_data


@pytest.fixture(scope="session")
def ec2_instance_config(request, environ: environ) -> dict:
    """Fixture return dict with test aws instance data"""

    return get_config_data(os.path.join(request.config.rootdir.strpath, "aws_tests",
                                        environ.get_expected_data()))

@pytest.fixture(scope="function")
def expected_ec2_instance_state(ec2_instance_config: dict) -> str:
    """Fixture returns a <state> value from a dictionary of expected data."""

    return ec2_instance_config.get("state")


@pytest.fixture(scope="function")
def expected_ec2_instance_instance_type(ec2_instance_config: dict) -> str:
    """Fixture returns a <instance_type> value from a dictionary of expected data."""

    return ec2_instance_config.get("instance_type")


@pytest.fixture(scope="function")
def expected_ec2_instance_network_interfaces(ec2_instance_config: dict) -> int:
    """Fixture returns a <network_interfaces_number> value from a dictionary of expected data."""

    return ec2_instance_config.get("network_interfaces_number")


@pytest.fixture(scope="function")
def expected_ec2_instance_private_ip_address(ec2_instance_config: dict) -> str:
    """Fixture returns a <network_interface_ips> value from a dictionary of expected data."""

    return ec2_instance_config.get("network_interface_ips")


@pytest.fixture(scope="function")
def expected_ec2_instance_root_device(ec2_instance_config: dict) -> str:
    """Fixture returns a <root_device> value from a dictionary of expected data."""

    return ec2_instance_config.get("root_device")


@pytest.fixture(scope="function")
def expected_ec2_instance_root_device_type(ec2_instance_config: dict) -> str:
    """Fixture returns a <root_device_type> value from a dictionary of expected data."""

    return ec2_instance_config.get("root_device_type")


@pytest.fixture(scope="function")
def expected_ec2_instance_key_pair_name(ec2_instance_config: dict) -> str:
    """Fixture returns a <instance_ssh_key_name> value from a dictionary of expected data."""

    return ec2_instance_config.get("instance_ssh_key_name")


@pytest.fixture(scope="function")
def expected_ec2_instance_tags(ec2_instance_config: dict) -> int:
    """Fixture returns a <instance_tags_count> value from a dictionary of expected data."""

    return ec2_instance_config.get("instance_tags_count")


@pytest.fixture(scope="function")
def expected_ec2_instance_tag_name(ec2_instance_config: dict) -> str:
    """Fixture returns a <instance_tag_name> value from a dictionary of expected data."""

    return ec2_instance_config.get("instance_tag_name")
