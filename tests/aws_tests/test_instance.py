"""Tests for AWSInstance class"""


def test_state_attr(expected_ec2_instance_state, ec2_instance_state):
    assert expected_ec2_instance_state == ec2_instance_state, "Error in <state> assertion!"


def test_instance_type_attr(expected_ec2_instance_instance_type, ec2_instance_instance_type):
    assert expected_ec2_instance_instance_type == ec2_instance_instance_type, \
                 "Error in <instance_type> assertion!"


def test_network_interfaces_attr(expected_ec2_instance_network_interfaces, ec2_instance_network_interfaces):
    assert expected_ec2_instance_network_interfaces == ec2_instance_network_interfaces, \
        "Error in <network_interfaces> assertion!"


def test_private_ip_address_attr(expected_ec2_instance_private_ip_address, ec2_instance_private_ip_address):
    assert expected_ec2_instance_private_ip_address == ec2_instance_private_ip_address, \
        "Error in <private_ip_address> assertion!"


def test_root_device_attr(expected_ec2_instance_root_device, ec2_instance_root_device):
    assert expected_ec2_instance_root_device == ec2_instance_root_device, "Error in <root_device> assertion!"


def test_root_device_type_attr(expected_ec2_instance_root_device_type, ec2_instance_root_device_type):
    assert expected_ec2_instance_root_device_type == ec2_instance_root_device_type, \
        "Error in <root_device_type> assertion!"


def test_key_pair_name_attr(expected_ec2_instance_key_pair_name, ec2_instance_key_pair_name):
    assert expected_ec2_instance_key_pair_name == ec2_instance_key_pair_name, \
                 "Error in <key_pair_name> assertion!"


def test_instance_tags_attr(expected_ec2_instance_tags, ec2_instance_tags):
    assert expected_ec2_instance_tags == ec2_instance_tags,  "Error in <instance_tags> assertion!"


def test_instance_tag_name_attr(expected_ec2_instance_tag_name, ec2_instance_tag_name):
    assert expected_ec2_instance_tag_name == ec2_instance_tag_name, \
        "Error in <instance_tag_name> assertion!"
