"""Conftest for aws resources"""

from fixtures.environ_fixtures import environ
from fixtures.aws_cloud.aws_fixtures import *
from fixtures.aws_cloud.expected_config_fixtures import *