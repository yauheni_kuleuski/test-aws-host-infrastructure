"""Tests for LinuxHost class"""


def test_linux_host_cpu(expected_linux_host_cpu, linux_host_cpu):
    assert expected_linux_host_cpu == linux_host_cpu, "Error in <cpu> assertion!"


def test_linux_host_ram(expected_linux_host_ram, linux_host_ram):
    assert expected_linux_host_ram == linux_host_ram, "Error in <ram> assertion!"


def test_linux_host_disk_size(expected_linux_host_disk_size, linux_host_disk_size):
    assert expected_linux_host_disk_size == linux_host_disk_size, "Error in <disk_size> assertion!"


def test_linux_host_network_interfaces(expected_linux_host_network_interfaces, linux_host_network_interfaces):
    assert expected_linux_host_network_interfaces == linux_host_network_interfaces, \
                                                   "Error in <network_interfaces> assertion!"


def test_linux_host_running_service(expected_linux_host_running_service, linux_host_running_service):
    assert expected_linux_host_running_service == linux_host_running_service, \
                                                   "Error in <running_service> assertion!"


def test_linux_host_enabled_service(expected_linux_host_enabled_service, linux_host_enabled_service):
    assert expected_linux_host_enabled_service == linux_host_enabled_service, \
                                                   "Error in <enabled_service> assertion!"
