"""This module contains fixtures for the LinuxHost class object"""

import os
from typing import Optional

import pytest

from fixtures.environ_fixtures import environ
from models.hosts.linux_host_infra import LinuxHost


@pytest.fixture(scope="session")
def linux_host(environ: environ) -> Optional[LinuxHost]:
    """Fixture for receiving an object LinuxHost"""

    path_to_ssh_key = os.path.expanduser(environ.get_ssh_key_path()) if environ.get_ssh_key_path() else None
    return LinuxHost(environ.get_linux_host_username(), environ.get_linux_host_ip(), path_to_ssh_key)


@pytest.fixture(scope="function")
def linux_host_cpu(linux_host: LinuxHost) -> int:
    """Fixture returns the <cpu> value from the LinuxHost object."""

    return linux_host.cpu


@pytest.fixture(scope="function")
def linux_host_ram(linux_host: LinuxHost) -> int:
    """Fixture returns the <ram> value from the LinuxHost object."""

    return linux_host.ram


@pytest.fixture(scope="function")
def linux_host_network_interfaces(linux_host: LinuxHost) -> int:
    """Fixture returns the <network_interfaces> value from the LinuxHost object."""

    return len(linux_host.network_interfaces)


@pytest.fixture(scope="function")
def linux_host_disk_size(linux_host: LinuxHost) -> int:
    """Fixture returns the <disk_size> value from the LinuxHost object."""

    return linux_host.disk_size


@pytest.fixture(scope="function")
def linux_host_running_service(linux_host: LinuxHost) -> bool:
    """Fixture returns the <True> value from the LinuxHost object if the checked service is started."""

    return linux_host.is_service_running("chronyd")


@pytest.fixture(scope="function")
def linux_host_enabled_service(linux_host: LinuxHost) -> bool:
    """Fixture returns the <True> value from the LinuxHost object if the checked service is enabled."""

    return linux_host.is_service_enabled("chronyd")
