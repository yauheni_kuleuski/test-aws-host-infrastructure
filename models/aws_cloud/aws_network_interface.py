"""
This module contains class which provides collection of complete information about "network_interface" instance.
"""

from typing import Optional

from .aws_resource import AWSResource


class AWSNetworkInterface(AWSResource):
    """The class contains all the necessary attributes of the "network_interface" instance."""

    def __init__(self, network_interface_id: str):
        """
        :param network_interface_id: String for operations with instance.
        """

        super(AWSNetworkInterface, self).__init__()
        self.network_interface_id = network_interface_id
        self.__instance = self.resource.NetworkInterface(self.network_interface_id)
        self.tags = self.__get_tags()
        self.tenant = self.__get_tenant()
        self.private_ip_address = self.__instance.private_ip_address
        self.interface_type = self.__instance.interface_type
        self.vpc_id = self.__instance.vpc_id

    def __get_tags(self) -> dict:
        """Return dict object with {Key: Value} parameters. Example: {'Tenant': 'tools', 'Name': 'report_portal'}"""

        return {tag["Key"]: tag["Value"] for tag in self.__instance.tag_set} if self.__instance.tag_set else dict()

    def __get_tenant(self) -> Optional[str]:
        """Return value from self.__instance.vpc.tags list. Example: "tools" """

        if self.__instance.vpc.tags:
            for tag in self.__instance.vpc.tags:
                if tag["Key"] == "Tenant":
                    return tag["Value"]
        return None
