"""This module contains class which provides collection of complete information about "volume" instance."""

from .aws_resource import AWSResource


class AWSVolume(AWSResource):
    """The class contains all the necessary attributes of the "volume" instance."""

    def __init__(self, volume_id: str):
        """
        :param volume_id: String for operations with Volume.
        """

        super(AWSVolume, self).__init__()
        self.volume_id = volume_id
        self.__instance = self.resource.Volume(self.volume_id)
        self.tags = self.__get_tags()
        self.size = self.__instance.size
        self.volume_type = self.__instance.volume_type
        self.state = self.__instance.state

    def __get_tags(self) -> dict:
        """Return dict object with {Key: Value} parameters. Example: {'Tenant': 'tools', 'Name': 'report_portal'}"""

        return {tag["Key"]: tag["Value"] for tag in self.__instance.tags} if self.__instance.tags else dict()
