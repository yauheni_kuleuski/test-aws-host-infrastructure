"""A module for presenting the logic of testing."""

import logging


def assert expected_data, object_data, error_message):
    """
    The function describes the logic of the test with the result logging.

    :param expected_data: The expected data is transmitted.
    :param object_data: Data is transferred from the object.
    :param error_message: Message in case of error.
    """

    assert expected_data == object_data, error_message
    logging.info("ASSERT %s -> %s" % (expected_data, object_data))
