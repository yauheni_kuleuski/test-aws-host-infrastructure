"""Module which importing all needed fixtures for starting testing."""

from fixtures.hosts.linux_host_fixtures import *
from fixtures.hosts.expected_config_fixtures import *