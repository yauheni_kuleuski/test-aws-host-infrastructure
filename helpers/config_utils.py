"""Module with functions assistants"""

import json
import logging
from typing import Optional


def get_config_data(config_file_path: str) -> Optional[dict]:
    """
    The function returns the read data from the json file.
    In the enter we get path to expected_data.json file with test data.
    """

    with open(config_file_path) as file:
        config_file_data = json.load(file)
    if config_file_data:
        return config_file_data
    else:
        logging.info("failed to load data from %s" % config_file_path)
        return dict()
