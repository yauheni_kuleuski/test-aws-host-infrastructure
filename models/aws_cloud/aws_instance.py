"""This module contains class which provides collection of complete information about "ec2" instance."""

from typing import List

from .aws_network_interface import AWSNetworkInterface
from .aws_resource import AWSResource
from .aws_volume import AWSVolume


class AWSInstance(AWSResource):
    """The class contains all the necessary attributes of the "ec2" instance."""

    def __init__(self, instance_id: str):
        """
        :param instance_id: String for operations with instance.
        """

        super(AWSInstance, self).__init__()
        self.instance_id = instance_id
        self.__instance = self.resource.Instance(instance_id)
        self.image_id: str = self.__instance.image_id
        self.instance_type: str = self.__instance.instance_type
        self.private_ip_address: str = self.__instance.private_ip_address
        self.public_ip_address: str = self.__instance.public_ip_address
        self.root_device: str = self.__instance.vpc.id
        self.root_device_type: str = self.__instance.root_device_type
        self.key_pair_name: str = self.__instance.key_pair.key_name
        self.state = self.__get_state()
        self.tags = self.__get_tags()
        self.security_groups = self.__get_security_groups()
        self.volumes = self.__get_volumes()
        self.network_interfaces = self.__get_network_interfaces()

    def __get_state(self) -> str:
        """Return value from self.__instance.state dict. Example: "running"
        """

        return self.__instance.state["Name"]

    def __get_tags(self) -> dict:
        """ Return dict object with {Key: Value} parameters. Example: {'Tenant': 'tools', 'Name': 'report_portal'}"""

        return {i["Key"]: i["Value"] for i in self.__instance.tags}

    def __get_security_groups(self) -> dict:
        """
        Return dict object with {GroupName: GroupId} parameters. Example: {'whitelist-lgi': 'sg-0d516000f11e91e89'}
        """

        return {group["GroupName"]: group["GroupId"] for group in self.__instance.security_groups}

    def __get_volumes(self) -> List[AWSVolume]:
        """
        Return list object with AWSVolume objects inside.
        """

        return [AWSVolume(volume.id) for volume in self.__instance.volumes.all()]

    def __get_network_interfaces(self) -> List[AWSNetworkInterface]:
        """
        Return list object with AWSNetworkInterface objects inside.
        """

        return [AWSNetworkInterface(interface.id) for interface in self.__instance.network_interfaces]
