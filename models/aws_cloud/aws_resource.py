"""This module contains Amazon standard Resource description"""

import boto3


class AWSResource:
    """
    Base class for creating AWS "ec2" instance.
    """

    def __init__(self, region_name: str = "eu-central-1"):
        """
        :param region_name: indicates the region for working with instances. Parameter with default value.
        As default -> "eu-central-1".
        """

        self.resource_name = "ec2"
        self.resource = boto3.resource(self.resource_name, region_name=region_name)
        self.client = boto3.client(self.resource_name, region_name=region_name)
