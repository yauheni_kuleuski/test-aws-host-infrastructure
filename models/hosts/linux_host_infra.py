"""Module for testing infrastructure with testinfra library"""

import logging
from typing import Optional

import testinfra


class HostNetworkInterface:
    """Class provides information about network interfaces remote host"""

    def __init__(self, name: str, host: testinfra.host.Host):
        """
        :param name: Takes an intarface name parameter.
        :param host: Takes an input testinfra.host.Host object.
        """

        self.name = name
        self.__interface = host.interface(name)

    def is_exists(self) -> Optional[bool]:
        """Check if there is such an interface. Return True if it's truth"""

        is_exists_variable =  self.__interface.exists
        if type(is_exists_variable) == bool:
            return is_exists_variable
        else:
            logging.info("Failed to get value -> is_exists_variable")
            return None

    def speed(self) -> Optional[int]:
        """Internet connection speed. Example: 1000"""

        speed_value = self.__interface.speed
        if type(speed_value) == int and speed_value:
            return speed_value
        else:
            logging.info("Failed to get value -> speed_value")
            return None

    def addresses(self) -> Optional[list]:
        """
        Method return list of ip and mac addresses.
        Example: ['10.0.1.10', 'fe35::334e:gt06:498f:3f41']
        """

        interface_addresses = self.__interface.addresses
        if type(interface_addresses) == list and interface_addresses:
            return interface_addresses
        else:
            logging.info("Failed to get value -> speed_value")
            return None


class LinuxHost:
    """The class provides complete Linux server information."""

    def __init__(self, username: str, ip_address: str, path_to_ssh_key):
        """
        :param username: Under which user is the connection.
        :param ip_address: Remote host IP address.
        :param path_to_ssh_key: A key needed to connect, if any.
        """

        self.__backend = "ssh://%s@%s" % (username, ip_address)
        self.__host = testinfra.get_host(self.__backend, ssh_identity_file=path_to_ssh_key)
        self.host_is_avalible = self.__check_host_avalible()
        self.cpu = self.__get_cpu()
        self.ram = self.__get_ram()
        self.disk_size = self.__get_disk_size()
        self.network_interfaces = [HostNetworkInterface(interface, self.__host) for interface in
                                   self.__get_network_interfaces() if HostNetworkInterface(interface,
                                                                                           self.__host).is_exists()]

    def __check_host_avalible(self) -> bool:
        """Method returns True if remote server is available."""

        try:
            self.__host.run("uptime").stdout
        except Exception as error:
            assert False, "Host is not avalible.\n%s" % error
        return True

    def __get_cpu(self) -> Optional[int]:
        """Method returns CPU cores number of the linux server. Example: 1"""

        cpu_value = int(self.__host.run("nproc").stdout)
        if type(cpu_value) == int and cpu_value != 0:
            return cpu_value
        else:
            logging.info("Failed to get value -> cpu")
            return None

    def __get_ram(self) -> Optional[int]:
        """Method returns RAM size (Mb) of the linux server. Example: 990"""

        ram_value = int(self.__host.run("free -m | grep Mem | awk '{print $2}'").stdout)
        if type(ram_value) == int and ram_value != 0:
            return int(ram_value)
        else:
            logging.info("Failed to get value -> ram")
            return None

    def __get_disk_size(self) -> Optional[int]:
        """Method returns partition size (Gb) of the linux server. Example: 8"""

        disk_size_value = int(self.__host.run("lsblk | grep disk | awk '{print $4}'").stdout.replace('G', ''))
        if type(disk_size_value) == int and disk_size_value != 0:
            return int(disk_size_value)
        else:
            logging.info("Failed to get value -> disk_size")
            return None

    def __get_network_interfaces(self) -> Optional[list]:
        """Method returns a list with names of network interfaces of the server. Example: ['enp0s3','enp0s8', 'lo']"""

        network_interfaces_value = self.__host.run("ls -A /sys/class/net").stdout.split()
        if network_interfaces_value:
            return network_interfaces_value
        else:
            logging.info("Failed to get value -> network_interfaces")
            return list()

    def is_service_running(self, service: str) -> Optional[bool]:
        """Method returns True if service is running"""

        service_running = self.__host.service(service).is_running
        if type(service_running) == bool:
            return service_running
        else:
            logging.info("Failed to get value -> is_service_running")
            return None

    def is_service_enabled(self, service: str) -> Optional[bool]:
        """Method returns True if service is enabled"""

        service_enabled = self.__host.service(service).is_enabled
        if type(service_enabled) == bool:
            return service_enabled
        else:
            logging.info("Failed to get value -> is_service_enabled")
            return None

if __name__ == "__main__":
    host = LinuxHost("root", "192.168.0.206", None)
    result = host._LinuxHost__host.run("[ -d $HOME/.ssh ] || mkdir $HOME/.ssh; cd $HOME/.ssh && ssh-keygen -f id_rsa -N ''")
    print(result)
