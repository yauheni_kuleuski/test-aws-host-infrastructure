Yauheni Kuleuski Task 3 - IN PROGRESS
=======================
Написать класс LinuxHost, который:
 - на вход будет принимать username, ip_address и path_to_ssh_key.
 - иметь методы для того чтобы узнать сколько у данного хоста CPU, RAM, disk size, какие у него нетворк интерфейсы, какие запущены процессы и сервисы
 - создать тесты на проверку инфраструктурных параметров хоста. Хост для тестирования - ec2-user@ip-172-31-47-174. Ключ скину в личку каждому, сохранить его под названием jenkins_access
 Ожидаемые результаты:
  - cpu_cores >= 2
  - ram_size >= 2 Gb
  - network_interfaces_count = 1
  - / partition_size >= 10 Gb
  - chronyd process should be running
  - chronyd.service  service should be enabled


Yauheni Kuleuski Task 2 - DONE
=======================

Using your wrapper classes you should test AWS Instance(take instance id from previous task).
Expected results for testing(provide expected results for your instance by yourself):
•    State=running
•    Instance_type= r5.xlarge
•    Network_interfaces_number=3
•    Network interface ips= 172.23.87.155, 100.96.4.63, 172.23.91.207
•    Root block device size= 500 Gb
•    Root block device type=HDD
•    Instance ssh key name=jenkins_access
•    Instance tags count = 5
•    Instance tag name= MT_tst_lincbe_02   

What should be done:
•    All preparation steps should be done in fixtures. For example: fixture with name instance_tags should provide only instance tags to the test.
•    Test should not contain any logic, it should have only assertion.
•    Use logging in your tests.
•    Divide your project into separate modules – tests, fixtures and helpers

project/
├── fixtures
│   └── aws_fixtures.py
├── models
│   └── aws_cloud
│       ├── ec2_instance.py
│       ├── network_interface_resource.py
│       └── volume_resource.py
└── tests
    └── aws_tests
        ├── test_instance.py
•    Expected results data should be stored in config file


Yauheni Kuleuski Task 1 - DONE
=======================

Get information about AWS ec2 instance using boto3. 
Create Python wrapper classes for EC2Instance, EBSVolume and NetworkInterface which would contain all needed attributes for appropriate class description.

instance-id: i-06007db6d3063ca53

List of attributes:
EC2Instance:
 - tags
 - image_id
 - private ip address
 - state
 - volumes
 - public ip address
 - security groups

 EBSVolume:
 - tags
 - size
 - type
 - state

NetworkInterface:
 - tags
 - tenant
 - ip address
