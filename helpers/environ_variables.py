"""Module for working with global variables."""

import logging
import os


class EnvironVariables:
    """Class provides access to global variables of the operating system in one class"""

    @staticmethod
    def get_variable(variable: str):
        """
        :param variable: In the enter we get global os variable. Example: LINUX_HOST_IP
        """

        if os.environ.get(variable):
            return os.environ.get(variable)
        else:
            logging.info("Variable %s not found, returned 'None' " % variable)
            return None

    @classmethod
    def get_instance_id(cls):
        """Method return <TEST_INSTANCE_ID> environ variable."""

        return cls.get_variable("TEST_INSTANCE_ID")

    @classmethod
    def get_linux_host_username(cls):
        """Method return <LINUX_HOST_USERNAME> environ variable."""

        return cls.get_variable("LINUX_HOST_USERNAME")

    @classmethod
    def get_linux_host_ip(cls):
        """Method return <LINUX_HOST_IP> environ variable."""

        return cls.get_variable("LINUX_HOST_IP")

    @classmethod
    def get_ssh_key_path(cls):
        """Method return <PATH_TO_SSH_KEY> environ variable."""

        return cls.get_variable("PATH_TO_SSH_KEY")

    @classmethod
    def get_expected_data(cls):
        """Method return <EXPECTED_DATA_JSON> environ variable."""

        return cls.get_variable("EXPECTED_DATA_JSON")
